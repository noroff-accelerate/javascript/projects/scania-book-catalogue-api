const { Model, DataTypes } = require('sequelize');
const { db } = require('../config/db.config');
const bcrypt = require('bcrypt');

class User extends Model {

    static generatePassword(plainPassword) {
        return bcrypt.hashSync(plainPassword, parseInt(process.env.HASH_ROUNDS));
    }

    static comparePassword(plainPassword, hashPassword) {
        return bcrypt.compareSync(plainPassword, hashPassword);
    }
}

User.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    full_name: DataTypes.STRING,
    email: DataTypes.STRING,
    last_login: DataTypes.STRING,
    active: {
        type: DataTypes.SMALLINT,
        defaultValue: 1
    },
    password: DataTypes.STRING
}, {
    sequelize: db,
    modelName: 'user'
});

module.exports = User;