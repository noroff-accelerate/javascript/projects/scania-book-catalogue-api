const { Model, DataTypes } = require('sequelize');
const { db } = require('../config/db.config');
const bcrypt = require('bcrypt');


class UserSession extends Model {
    static generateToken() {
        return bcrypt.hashSync(process.env.HASH_SECRET_TOKEN, parseInt(process.env.HASH_ROUNDS));
    }
}

UserSession.init({
    user_session_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_id: DataTypes.INTEGER,
    token: DataTypes.STRING,
    active: {
        type: DataTypes.SMALLINT,
        defaultValue: 1
    }
}, {
    sequelize: db,
    modelName: 'user_session'
});

module.exports = UserSession;