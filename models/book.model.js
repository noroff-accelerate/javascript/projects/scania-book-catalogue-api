const { Model, DataTypes } = require('sequelize');
const { db } = require('../config/db.config');

class Book extends Model {}

Book.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    isbn: DataTypes.STRING,
    authors: DataTypes.STRING,
    cover: DataTypes.STRING,
    stock: DataTypes.INTEGER,
    active: {
        type: DataTypes.SMALLINT,
        defaultValue: 1
    },
    user_id: DataTypes.INTEGER
}, {
    sequelize: db,
    modelName: 'book'
});

module.exports = Book;