const { Book, User } = require('../models/models');
const router = require('express').Router();
const Response = require('../models/response.model');

router.get('/', async (req, res) => {

    const r = new Response();

    const { user_id } = req;
    

    try {
        r.data = await Book.findAll({
            where: {
                active: 1,
                user_id: user_id
            },
            attributes: ['id', 'title', 'authors','description', 'cover', 'stock', 'isbn', 'created_at', 'updated_at']
        });
    } catch (e) {
        r.setError(e);
    }


    return res.status(r.status).json(r);

});

router.get('/:book_id', async (req, res) => {

    const r = new Response();
    const { book_id } = req.params;
    const { user_id } = req;

    try {

        if (!book_id) {
            throw ({ status: 400, message: 'No Book id was provided' });
        }

        r.data = await Book.findOne({
            where: {
                id: book_id,
                active: 1,
                user_id: user_id
            },
            attributes: ['id', 'title', 'description', 'created_at', 'updated_at']
        });

    } catch (e) {
        r.setError(e);
    }

    return res.status(r.status).json(r);

});

router.post('/', async (req, res) => {

    const r = new Response();
    const { book } = req.body;


    try {

        if (!book) {
            throw ({ status: 400, message: 'No Book information was provided.' });
        }

        if (!book.title) {
            throw ({ status: 400, message: 'The books need a title.' });
        }

        if (!book.isbn) {
            throw ({ status: 400, message: 'The isbn is required for a book.' });
        }

        if (!book.stock) {
            throw ({ status: 400, message: 'You did not specify how many stock items are being added.' });
        }

        if (!book.authors) {
            throw ({ status: 400, message: 'A book must have an author.' });
        }

        if (!book.cover) {
            book.cover = "https://sociology.indiana.edu/images/publications/book-cover-placeholder.jpg";
        }

        const hasBook = await Book.findAll({
            where: {
                user_id: req.user_id,
                title: book.title,
                isbn: book.isbn
            }
        });

        if (hasBook.length > 0) {
            throw({ status: 400, message: 'It looks like this book has already been added.' });
        }

        book.user_id = req.user_id;
        r.data = await Book.create(book, {
            attributes: {
                exclude: ['user_id']
            }
        });
        r.status = 201;

    } catch (e) {
        let error = e;
        if (typeof e == 'object' && e.hasOwnProperty('error') && e.errors.length > 0 && e.errors[0].message) {
            error = {
                message: e.errors[0].message,
                status: 400
            }
        }
        r.setError(error);
    }
    return res.status(r.status).json(r);
});

router.delete('/:book_id', async (req, res) => {
    const r = new Response();
    const { book_id } = req.params;

    try {
        const result = await Book.destroy({
            where: {
                id: book_id,
                user_id: req.user_id
            }
        });

        r.data = await Book.findAll({
            where: {
                active: 1,
                user_id: req.user_id
            }
        });

    } catch (e) {
       r.setError(e);
    } 

    return res.status(r.status).json(r);
})


module.exports = router;
